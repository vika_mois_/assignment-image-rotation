#include "image.h"
#include "bmp.h"
#include "transformations.h"
#include "utils.h"


int main(int argc, char **argv) {
    if (argc != 3) {
        fprintf(stderr, "Use ./image-transformer <source-image> <transformed-image>");
        return -1;
    }

    struct image img;

    FILE *in = open_file(argv[1], READ_FILE);
    enum read_status rs = from_bmp(in, &img);
    fclose(in);
    if (rs != READ_OK) {
        fprintf(stderr, "Error reading image: %d", rs);
        return rs;
    }

    struct image rotated = rotate(img);
    free_image(img);

    FILE *out = open_file(argv[2], WRITE_FILE);
    enum write_status ws = to_bmp(out, &rotated);
    fclose(out);
    if (ws != WRITE_OK) {
        fprintf(stderr, "Error writing image: %d", ws);
        return ws;
    }

    free_image(rotated);

    return 0;
}
