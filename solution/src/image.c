#include "image.h"

struct image alloc_image(uint64_t width, uint64_t height) {
    struct image img;
    img.width = width;
    img.height = height;
    img.data = malloc(width * height * sizeof(struct pixel));
    return img;
}

void free_image(struct image img) {
    free(img.data);
}

struct pixel *get_pixel(struct image const *img, uint64_t x, uint64_t y) {
    return &img->data[y * img->width + x];
}

void set_pixel(struct image *img, uint64_t x, uint64_t y, struct pixel const *p) {
    img->data[y * img->width + x] = *p;
}

struct image null_image(void){
    struct image img;
    img.width = 0;
    img.height = 0;
    img.data = NULL;
    return img;
}
