#include "bmp.h"

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

uint64_t get_padding(uint64_t width) {
    uint64_t bits = width * sizeof(struct pixel);
    bits %= 4;
    return (4 - bits) % 4;
}

enum read_status read_header(FILE *in, struct bmp_header *out_header) {
    struct bmp_header header;
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1)
        return READ_INVALID_HEADER;

    if (fseek(in, (long int) header.bOffBits, SEEK_SET))
        return READ_INVALID_HEADER;

    *out_header = header;

    return READ_OK;
}

enum read_status check_header(struct bmp_header header) {
    if (header.bfType != 0x4D42)
        return READ_INVALID_SIGNATURE;
    if (header.biBitCount != 24)
        return READ_INVALID_BITS;
    if (header.biCompression != 0)
        return READ_INVALID_HEADER;

    return READ_OK;
}

enum read_status from_bmp(FILE *in, struct image *img) {
    if (in == NULL)
        return READ_OPEN_FILE_ERROR;

    struct bmp_header header;
    enum read_status status = read_header(in, &header);
    if (status != READ_OK)
        return status;

    status = check_header(header);
    if (status != READ_OK)
        return status;

    struct image temp;
    temp = alloc_image(header.biWidth, header.biHeight);

    const long int PADDING = (long int) get_padding(temp.width);

    for (uint64_t i = 0; i < temp.height; i++) {
        for (uint64_t j = 0; j < temp.width; j++)
            if (fread(&temp.data[i * temp.width + j], sizeof(struct pixel), 1, in) != 1) {
                status = READ_INVALID_FILE;
                break;
            }


        if (PADDING)
            if (fseek(in, PADDING, SEEK_CUR) != 0) {
                status = READ_INVALID_FILE;
                break;
            }

    }

    if (status == READ_OK) {
        *img = temp;
    } else {
        free_image(temp);
        *img = null_image();
    }

    return status;
}

enum write_status write_header(FILE *out, struct image const *img) {
    struct bmp_header header;
    header.bfType = 0x4D42;
    header.bfileSize = sizeof(struct bmp_header) + img->width * img->height * sizeof(struct pixel) +
                       get_padding(img->width) * img->height;
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = 40;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = 0;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1)
        return HEADER_WRITE_ERROR;
    else
        return WRITE_OK;

}

enum write_status to_bmp(FILE *out, struct image const *img) {
    if (out == NULL)
        return WRITE_OPEN_FILE_ERROR;

    const uint64_t PADDING = get_padding(img->width);
    if (write_header(out, img) != WRITE_OK)
        return HEADER_WRITE_ERROR;

    for (uint64_t i = 0; i < img->height; i++) {
        for (uint64_t j = 0; j < img->width; j++)
            if (fwrite(&img->data[i * img->width + j], sizeof(struct pixel), 1, out) != 1)
                return WRITE_ERROR;


        if (PADDING) {
            uint8_t padding[3] = {0, 0, 0};
            if (fwrite(padding, PADDING, 1, out) != 1)
                return WRITE_ERROR;

        }
    }

    return WRITE_OK;
}
