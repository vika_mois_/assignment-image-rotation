#include "utils.h"

#include <stdio.h>

FILE *open_file(char const *filename, enum FILE_MODE mode) {
    FILE *file = NULL;

    if (mode == READ_FILE)
        file = fopen(filename, "rb");
    else if (mode == WRITE_FILE)
        file = fopen(filename, "wb");

    return file;
}

