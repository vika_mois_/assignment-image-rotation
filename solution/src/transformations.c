#include "image.h"
#include "transformations.h"

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate(struct image const source) {
    struct image img = alloc_image(source.height, source.width);

    for (size_t x = 0; x < source.width; x++)
        for (size_t y = 0; y < source.height; y++) {
            struct pixel *p = get_pixel(&source, x, y);
            set_pixel(&img, source.height - y - 1, x, p);
        }

    return img;
}
