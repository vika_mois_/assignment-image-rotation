#ifndef ASSIGNMENT_IMAGE_ROTATION_UTILS_H
#define ASSIGNMENT_IMAGE_ROTATION_UTILS_H

#include <stdio.h>

enum FILE_MODE{
    READ_FILE,
    WRITE_FILE
};

FILE *open_file(char const *filename, enum FILE_MODE mode);

#endif //ASSIGNMENT_IMAGE_ROTATION_UTILS_H
