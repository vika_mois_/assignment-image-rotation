#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <stdint.h>

#include <stdio.h>
#include <stdlib.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct image alloc_image(uint64_t width, uint64_t height);

struct image null_image(void);

struct pixel *get_pixel(struct image const *img, uint64_t x, uint64_t y);

void set_pixel(struct image *img, uint64_t x, uint64_t y, struct pixel const *p);

void free_image(struct image img);


#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
